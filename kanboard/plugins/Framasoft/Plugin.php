<?php

namespace Kanboard\Plugin\Framasoft;

use Kanboard\Core\Plugin\Base;

class Plugin extends Base
{
    public function initialize()
    {
        $this->setContentSecurityPolicy(array(
            'default-src' => "'self' https://framasoft.org/ 'unsafe-inline'",
            'script-src' => "'self' https://framasoft.org https://stats.framasoft.org/ 'unsafe-inline'",
            'font-src' => "'self' https://framasoft.org/",
            'img-src' => '* data:',
        ));
        $this->template->setTemplateOverride('layout', 'Framasoft:layout');
    }

    public function getPluginName() {
        return 'Framasoft';
    }

    public function getPluginAuthor() {
        return 'Marien Fressinaud';
    }

    public function getPluginVersion() {
        return '1.0';
    }

    public function getPluginDescription() {
        return '"Fizze" le thème de Kanboard';
    }

    public function getPluginHomepage() {
        return 'https://framagit.org/framasoft/Framaboard';
    }
}
