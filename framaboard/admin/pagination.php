<?php if ($paginator->number_pages() > 1) { ?>
<nav>
    <ul class="pagination">
        <li <?php echo !$paginator->has_previous_page() ? 'class="disabled"' : ''; ?>>
            <a href="?page=<?php echo $paginator->previous_page(); ?>" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>
        <?php foreach ($paginator->iter() as $page) { ?>
            <li <?php echo $paginator->is_current_page($page) ? 'class="active"' : ''; ?>>
                <a href="?page=<?php echo $page; ?>"><?php echo $page; ?></a>
            </li>
        <?php } ?>
        <li <?php echo !$paginator->has_next_page() ? 'class="disabled"' : ''; ?>>
            <a href="?page=<?php echo $paginator->next_page(); ?>" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    </ul>
</nav>
<?php } ?>
