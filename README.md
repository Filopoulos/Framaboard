[![](https://framagit.org/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# Framaboard

Ce dépot contient le code qui permet de faire tourner Framaboard.

Framaboard est une application de gestion de tâches basée sur [le logiciel Kanboard](https://github.com/kanboard/kanboard) de [Frédéric Guillot](https://github.com/fguillot). Il se base principalement sur l'ajout de deux fichiers spécifiques : `kanboard/config.php` (le fichier de configuration) et `framaboard/index.php` qui permet à des utilisateurs de s'enregistrer pour obtenir leur propre instance de Kanboard.

Si Framaboard n'utilise pas la possibilité qu'offre Kanboard de faire du multi-utilisateurs, c'est pour plusieurs raisons :

- Nous séparons les bases de données des utilisateurs ;
- Il est plus facile de repérer un abus ;
- La sauvegarde est facilitée pour les utilisateurs.

## Version supportée de Kanboard

La dernière version testée et utilisée avec Framaboard est Kanboard 1.0.33.

## Fonctionnement

Framaboard utilise une configuration spécifique pour Kanboard. Nous prendrons comme exemple le site http://board.exemple.

- http://board.exemple correspond à la page d'accueil de Framaboard, celle sur laquelle un utilisateur peut créer son compte. Par exemple, le compte "toto" ;
- Une fois le compte créé, le nouveau board est accessible grace à un sous-domaine. Par exemple, l'adresse http://toto.board.exemple ;
- Comme il n'existe qu'une seule instance de Kanboard installée, c'est le fichier `./kanboard/config.php` qui se charge ensuite de déterminer quelle base de données doit être chargée ;
- Les bases de données et fichiers des différents comptes se trouvent dans le répertoire `./users`.

## Installation

### Pré-requis

Comme expliqué plus haut, Framaboard se base sur des sous-domaines pour gérer les différents comptes. Aussi, il est nécessaire qu'un enregistrement DNS "wildcard" pointe sur votre serveur. Par exemple :

```
* IN CNAME board.exemple.
```

De plus, il est nécessaire de configurer votre serveur web correctement :

- Faites pointer l'adresse board.exemple sur le répertoire `./framaboard` ;
- Faites pointer **toutes** les adresses `*.board.exemple` sur le répertoire `./kanboard`.

Vous trouverez des exemples pour Apache et Nginx plus loin dans ce README.

### Étapes

Il ne reste plus que quelques étapes avant d'avoir un Framaboard qui fonctionne :

1. Téléchargez [la dernière version de Kanboard](http://kanboard.net/downloads) ;
2. Dézippez l'archive dans le répertoire `./kanboard` ;
3. Mettez le tout sur votre serveur ;
4. Assurez-vous que le serveur possède les droits en écriture sur `./users` et `./kanboard/data` ;
5. Éditez les fichiers `./common.php` (pour changer la valeur de `URL_BASE`) et `./kanboard/config.php` (principalement pour `MAIL_FROM`) ;
6. Vous avez fini !

Fini ne signifie pas sécurisé ! Pensez à mettre une authentification HTTP sur le répertoire `./framaboard/admin` sans quoi n'importe qui pourrait supprimer des comptes.

## Configuration serveur

Voici des *exemples* de fichiers de configuration. Ils peuvent (et doivent !) être adaptés à votre situation.

### Apache

```
<VirtualHost *:80>
    ServerAdmin contact@your-server.tld
    DocumentRoot /var/www/html/Framaboard/framaboard
    ServerName your-server.tld

    <Directory /var/www/html/Framaboard/framaboard>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>

<VirtualHost *:80>
    ServerAdmin contact@your-server.tld
    DocumentRoot /var/www/html/Framaboard/kanboard
    # Le ServerName doit être différent de your-server.tld (qui a
    # déjà été indiqué plus haut) ou sinon vous allez faire face à de
    # petits soucis. Mettez donc ce que vous voulez :).
    ServerName something.your-server.tld
    ServerAlias *.your-server.tld

    <Directory /var/www/html/Framaboard/kanboard>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>

```

### Nginx

Avec la configuration suivante, il est probable que vous deviez décommenter la dernière ligne du fichier `./kanboard/config.php` :

```php
$_SERVER['PHP_SELF'] = '/index.php';
```

Le fichier de configuration Nginx :

```
server {
    listen 80;

    server_name your-server.tld *.your-server.tld;
    root /var/www/html/Framaboard/;
    index index.php index.html index.htm;

    location ~ ^.+?\.php(/.*)?$ {
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    }

    # And the magic happened!
    # Concrêtement on regarde si le host demandé correspond a l'host
    # principal : si oui on charge le répertoire ./framaboard, sinon
    # c'est qu'il s'agit d'un sous-domaine, on charge alors le
    # répertoire ./kanboard
    location / {
        try_files $uri $uri/ /index.html;
        if ($host != "your-server.tld") {
            rewrite /(.*) /kanboard/$1 break;
        }
        if ($host = "your-server.tld") {
            rewrite /(.*) /framaboard/$1 break;
        }
    }
}
```

## Mise à jour de Kanboard

Régulièrement, il est nécessaire de mettre à jour de Kanboard pour suivre ses
évolutions. La tâche n'est pas encore facile car Framaboard se base beaucoup
sur les API internes de Kanboad, API qui évoluent à chaque nouvelle version. Il
est donc encore nécessaire de bien tester la création des comptes ainsi que la
connexion automatique après création et la mise à jour du mot de passe à partir
de l'administration.

Pour cela, rien de magique : il faut tester ces différentes fonctionnalités
avec les logs sous les yeux et adapter le code (d'expérience, principalement
dans `./common.php`).

Le ticket [#2698](https://github.com/kanboard/kanboard/issues/2698) sur le
bugtracker de Kanboard est censé répondre à cette problématique en introduisant
dans l'API **externe** de Kanboard les mécanismes nécessaires à la création
d'un compte.

Il n'existe pas aujourd'hui de tests pour assurer le bon fonctionnement de
l'application.

Il est aussi nécessaire de vérifier la bonne intégration de la Framanav avec la
nouvelle version de Kanboard. Un peu n'importe quoi peut se casser comme le
design de boutons, les modales qui sont décalées ou encore le header qui fait
n'importe quoi. Il est souvent nécessaire de faire des ajustements dans le
fichier [board.css](https://git.framasoft.org/framasoft/framanav/blob/master/ext/board.css).

## Mise à jour de Framaboard

Toutes les informations pour mettre à jour le service Framaboard sont
disponibles sur le wiki de l'asso.
